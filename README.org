* Sync Lichess
** Installation
   #+begin_src bash
go install gitlab.com/reedrichards/sync-lichess
   #+end_src
** Configuration
   ~sync-lichess~ uses the following environment variables as configuration
*** ~LICHESS_TOKEN~
    token used for authentication with lichess.org [[https://lichess.org/account/oauth/token][Generate a personal API token]]
*** ~LICHESS_USERNAME~
    username of user that you would like to retrieve the games from
*** ~PGNS_DIRPATH~
    absolute path to directory to store pgn file, must have trailing slash
** Usage
    requirements:
      - go is already installed on your system
      - you have created a directory to save your pgns in already
*** Standalone Command
    configure your environment variables
    #+begin_src bash
export LICHESS_TOKEN=<YOUR_TOKEN>
export LICHESS_USERNAME=bvzz
export PGNS_DIRPATH=/home/rob/pgns
    #+end_src
    install the package
   #+begin_src bash
go install gitlab.com/reedrichards/sync-lichess
   #+end_src
   You can now run the program by typing its full path at the command line:
#+begin_src bash
$GOPATH/bin/sync-lichess
#+end_src
Or, as you have added $GOPATH/bin to your PATH, just type the binary name:
#+begin_src bash
sync-lichess
#+end_src
*** Kubernetes Cronjob
    create a namespace definition
~namespace.yaml~
    #+begin_src yaml
apiVersion: v1
kind: Namespace
metadata:
  name: sync-lichess
    #+end_src
    add the namespace to your cluster
    #+begin_src bash
kubectl apply -f namespace.yaml
    #+end_src
    create file called ~.env~ and populate it with your desired environment variables

~.env~
    #+begin_src conf
LICHESS_TOKEN=<YOUR_TOKEN>
LICHESS_USERNAME=bvzz
PGNS_DIRPATH=/home/rob/pgns
    #+end_src
    create a secret on your cluster using the ~.env~ file
    #+begin_src bash
kubectl create secret generic lichess-secrets --from-env-file=./.env --namespace sync-lichess
    #+end_src
    create a cronjob resource definition for your cluster using the secrets and namespace that we just created

~cronjob.yaml~
    #+begin_src yaml
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: sync-lichess
  namespace: sync-lichess
spec:
  # every day at 1 am
  schedule: "0 1 * * *"
  concurrencyPolicy: Replace
  jobTemplate:
    spec:
      template:
        spec:
          volumes:
            - name: lichess-volume
              hostPath:
                path: /mnt/data/pgns/
          containers:
            - name: sync-lichess
              # last known stable version
              # TODO create stable docker image
              image: docker.io/reedrichards/sync-lichess:70475835
              volumeMounts:
                - mountPath: /mnt/data/pgns
                  name: lichess-volume
              envFrom:
                - secretRef:
                    name: lichess-secrets
          restartPolicy: OnFailure
    #+end_src
    apply the cronjob
    #+begin_src bash
kubectl apply -f cronjob.yaml
    #+end_src
    The sync will now run every day at 1 am
