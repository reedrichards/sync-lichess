package main

import (
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/reedrichards/lichess"
)

var token = os.Getenv("LICHESS_TOKEN")
var username = os.Getenv("LICHESS_USERNAME")
var dirPath = os.Getenv("PGNS_DIRPATH")
var filepath = dirPath + username + ".pgn"

// sync all games for a user to a file
func main() {
	log.Println("fetching game data")
	gameData := lichess.GamesUser(username, token)

	log.Println("writing file: ", filepath)
	err := ioutil.WriteFile(filepath, gameData, 0666)
	if err != nil {
		log.Fatal(err)
	}
}
